var mysql = require('mysql');

var con = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "example",
    database: "rozglosnia"
});

const music_db = [];
con.connect(function(err) {
    if (err) throw err;
    con.query("SELECT * FROM music", function (err, result, fields) {
        if (err) throw err;
        music_db.push(result);
    });
});

exports.Music = function () {
    return music_db;
};
