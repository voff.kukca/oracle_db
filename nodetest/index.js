const express = require('express')
const ms = require('./getMusic')
var mysql = require('mysql');
var timeout = require('connect-timeout');
const app = express()

const PORT = 3000 || process.env.PORT

app.listen(PORT, () => console.log(`App is live on port ${PORT}`))

const path = require('path')

app.use(express.static(path.join(__dirname, 'public')))

app.get('/music', function(req, res){
    res.send(ms.Music());
});

const ws = require('ws');

const app2 = express();

// Set up a headless websocket server that prints any
// events that come in.
const wsServer = new ws.Server({ noServer: true });
wsServer.on('connection', socket => {
    socket.on('message', message => {
        var con = mysql.createConnection({
            host: "localhost",
            user: "root",
            password: "example",
            database: "rozglosnia"
        });
        //console.log((message.toString()))
        con.connect(function(err) {
            if (err) throw err;
            //let strcom = "SELECT * FROM music WHERE ID = " + toString(message);
            con.query("SELECT * FROM music WHERE ID='?'", message.toString(), function (err, result, fields) {
                if (err) throw err;
                var normalObj = Object.assign({}, result[0]);
                //console.log(normalObj);
                try {
                    var sql = "INSERT INTO queue (song_name, length, artist) VALUES ('" + normalObj["song_name"] + "', '" + normalObj["length"] + "', '" + normalObj["artist"] + "')";
                } catch (e) {
                    break;
                }
                //console.log(sql)
                con.query(sql, function (err, result) {
                    if (err) throw err;
                    console.log("1 record inserted, ID: " + result.insertId);
                });
                con.query("SELECT * FROM queue", function (err, result, fields) {
                    if (err) throw err;
                    console.log(JSON.stringify(result))
                    socket.send((JSON.stringify(result)))
                });
            });
        });
    });
});

// `server` is a vanilla Node.js HTTP server, so use
// the same ws upgrade process described here:
// https://www.npmjs.com/package/ws#multiple-servers-sharing-a-single-https-server
const server = app2.listen(8082);
server.on('upgrade', (request, socket, head) => {
    wsServer.handleUpgrade(request, socket, head, socket => {
        wsServer.emit('connection', socket, request);
    });
});



const wsServer2 = new ws.Server({ noServer: true });

const server2 = app2.listen(8083);
server2.on('upgrade', (request, socket, head) => {
    wsServer2.handleUpgrade(request, socket, head, socket => {
        wsServer2.emit('connection', socket, request);
    });
});

wsServer2.on('connection', socket => {
    socket.on('message', message => {
        console.log(message.toString());
        if(message.toString() === 'uwu') {
            var con = mysql.createConnection({
                host: "localhost",
                user: "root",
                password: "example",
                database: "rozglosnia"
            });
            //console.log((message.toString()))
            con.connect(function (err) {
                if (err) throw err;
                //let strcom = "SELECT * FROM music WHERE ID = " + toString(message);
                con.query("SELECT * FROM queue", function (err, result, fields) {
                    if (err) throw err;
                    socket.send((JSON.stringify(result)))
                });
            });
        }
        else {
            if (message.toString().includes("rejected")) {
                if (message.toString().includes("playlist")) {
                    var a = message.toString()
                    a = a.replace("rejected_from_playlist(", "");
                    a = a.replace(")", "");
                    var con = mysql.createConnection({
                        host: "localhost",
                        user: "root",
                        password: "example",
                        database: "rozglosnia"
                    });
                    con.connect(function (err) {
                        if (err) throw err;
                        var sql = "DELETE FROM playlist WHERE ID = '" + a + "'";
                        con.query(sql, function (err, result) {
                            if (err) throw err;
                            console.log("Number of records deleted: " + result.affectedRows);
                        });
                    });
                }
                else {
                    var a = message.toString()
                    a = a.replace("rejected(", "");
                    a = a.replace(")", "");
                    var con = mysql.createConnection({
                        host: "localhost",
                        user: "root",
                        password: "example",
                        database: "rozglosnia"
                    });
                    con.connect(function (err) {
                        if (err) throw err;
                        var sql = "DELETE FROM queue WHERE ID = '" + a + "'";
                        con.query(sql, function (err, result) {
                            if (err) throw err;
                            console.log("Number of records deleted: " + result.affectedRows);
                        });
                    });
                }
            }
            if (message.toString().includes("add_to_playlist")) {
                var a = message.toString()
                a = a.replace("add_to_playlist(", "");
                a = a.replace(")", "");
                var con = mysql.createConnection({
                    host: "localhost",
                    user: "root",
                    password: "example",
                    database: "rozglosnia"
                });
                //console.log((a))
                con.connect(function (err) {
                    if (err) throw err;
                    con.query("SELECT * FROM queue WHERE ID = '" + a + "'", function (err, result, fields) {
                        if (err) throw err;
                        var normalObj = Object.assign({}, result[0]);
                        //console.log(normalObj);

                        var sql = "INSERT INTO playlist (song_name, length, artist) VALUES ('" + normalObj["song_name"] + "', '" + normalObj["length"] + "', '" + normalObj["artist"] + "')";

                        //console.log(sql)
                        con.query(sql, function (err, result) {
                            if (err) throw err;
                            console.log("1 record inserted, ID: " + result.insertId);
                        });
                    });
                    var sql = "DELETE FROM queue WHERE ID = '" + a + "'";
                    con.query(sql, function (err, result) {
                        if (err) throw err;
                        console.log("Number of records deleted: " + result.affectedRows);
                    });
                });
            }
        }
    });
});


const wsServer3 = new ws.Server({ noServer: true });

const server3 = app2.listen(8084);
server3.on('upgrade', (request, socket, head) => {
    wsServer3.handleUpgrade(request, socket, head, socket => {
        wsServer3.emit('connection', socket, request);
    });
});

wsServer3.on('connection', socket => {
    socket.on('message', message => {
        console.log(message.toString());
        if (message.toString() === 'playlist') {
            var con = mysql.createConnection({
                host: "localhost",
                user: "root",
                password: "example",
                database: "rozglosnia"
            });
            //console.log((message.toString()))
            con.connect(function (err) {
                if (err) throw err;
                //let strcom = "SELECT * FROM music WHERE ID = " + toString(message);
                con.query("SELECT * FROM playlist", function (err, result, fields) {
                    if (err) throw err;
                    socket.send((JSON.stringify(result)))
                });
            });
        }
        if (message.toString() === 'now_playing') {
            var con = mysql.createConnection({
                host: "localhost",
                user: "root",
                password: "example",
                database: "rozglosnia"
            });
            console.log((message.toString()))
            con.connect(function (err) {
                if (err) throw err;
                //let strcom = "SELECT * FROM music WHERE ID = " + toString(message);
                var t = 0;/**/
                con.query("SELECT * FROM playlist LIMIT 1;", function (err, result, fields) {
                    //if (err) throw err;
                    if((JSON.stringify(result)).length === 2) {
                        socket.send(("no more"));
                        app2.use(timeout(10000));
                    }
                    else {
                        socket.send(JSON.stringify(result));
                        t = ((result)[0]["ID"]);
                        normalObj = (result)[0];
                        var con = mysql.createConnection({
                            host: "localhost",
                            user: "root",
                            password: "example",
                            database: "rozglosnia"
                        });
                        //console.log(t);
                        con.connect(function (err) {
                            //if (err) throw err;
                            console.log(t);
                            var sql = "DELETE FROM playlist WHERE ID = '" + parseInt(t) + "'";
                            con.query(sql, function (err, result) {
                                //if (err) throw err;
                                console.log("Number of records deleted: " + result.affectedRows);
                            });
                            sql = "DELETE FROM now_playing WHERE ID > 0";
                            con.query(sql, function (err, result) {
                                //if (err) throw err;
                                console.log("Number of records deleted: " + result.affectedRows);
                            });
                            sql = "INSERT INTO now_playing (song_name, length, artist) VALUES (?, ?, ?)";
                            let values = [(result)[0]["song_name"], (result)[0]["length"], (result)[0]["artist"]];
                            con.query(sql, values, function (err, result) {
                                //if (err) throw err;
                                console.log("Number of records deleted: " + result.affectedRows);
                            });
                        });
                    }
                   /* con.connect(function (err) {
                        //if (err) throw err;
                        var sql = "DELETE FROM now_playing WHERE ID > 0";
                        con.query(sql, function (err, result) {
                            //if (err) throw err;
                            console.log("Number of records deleted: " + result.affectedRows);
                        });
                        var sql = "INSERT INTO now_playing (song_name, length, artist) VALUES ('" + normalObj["song_name"] + "', '" + normalObj["length"] + "', '" + normalObj["artist"] + "')";
                        con.query(sql, function (err, result) {
                            //if (err) throw err;
                            console.log("Number of records deleted: " + result.affectedRows);
                        });
                        var sql = "DELETE FROM playlist WHERE ID = '" + t + "'";
                        con.query(sql, function (err, result) {
                            //if (err) throw err;
                            console.log("Number of records deleted: " + result.affectedRows);
                        });
                    });*/
                });
            });
        }
    });
});


