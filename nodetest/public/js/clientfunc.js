function getMusic() {
    select = document.getElementById('getMusic');
    select.innerHTML = '';
    fetch('/music')
        .then(function(response) {
            return response.json();
        })
        .then(function(myJson) {
            const obj = JSON.parse(JSON.stringify(myJson));
            for (const element of obj) {
                for (const monada of element) {
                    var opt = document.createElement('option');
                    opt.value = monada["ID"];
                    opt.innerHTML = monada["song_name"] + ' by ' + monada["artist"];
                    select.appendChild(opt);
                }
            }
        });
}

function sendMusic() {
    select = document.getElementById('getMusic');
    var value = select.value;
    console.log((value));

    const socket = new WebSocket('ws://' + window.location.hostname + ':8082');

    socket.addEventListener('open', (event) => {
        socket.send((value));
    });
    select2 = document.getElementById('queue_table');
    select2.innerHTML = '';
    socket.addEventListener('message', (event) => {
        if (event.data instanceof Blob) {
            reader = new FileReader();

            reader.onload = () => {
                console.log("Result: " + reader.result);
            };

            reader.readAsText(event.data);
        } else {
            //console.log("Result: " + event.data);
            const obj = JSON.parse((event.data));
            for (const monada of obj) {
                    // Create an empty <tr> element and add it to the 1st position of the table:
                    var row = select2.insertRow(0);

                    // Insert new cells (<td> elements) at the 1st and 2nd position of the "new" <tr> element:
                    var id = row.insertCell(0);
                    var name = row.insertCell(1);
                    var length = row.insertCell(2);
                    var artist = row.insertCell(3);

                    // Add some text to the new cells:
                    id.innerHTML = monada["ID"];
                    name.innerHTML = monada["song_name"];
                    length.innerHTML = monada["length"];
                    artist.innerHTML = monada["artist"];
            }
        }
    });/*

    fetch('http://localhost:3000/queue')
        .then(function(response) {
            return response.json();
        })
        .then(function(myJson) {
            const obj = JSON.parse(JSON.stringify(myJson));
            console.log(obj)

            for (const element of obj) {
                for (const monada of element) {
                    // Create an empty <tr> element and add it to the 1st position of the table:
                    var row = select2.insertRow(0);

                    // Insert new cells (<td> elements) at the 1st and 2nd position of the "new" <tr> element:
                    var id = row.insertCell(0);
                    var name = row.insertCell(1);
                    var length = row.insertCell(2);
                    var artist = row.insertCell(3);

                    // Add some text to the new cells:
                    id.innerHTML = monada["ID"];
                    name.innerHTML = monada["song_name"];
                    length.innerHTML = monada["length"];
                    artist.innerHTML = monada["artist"];
                }
            }
        });*/
}

function makeid(length) {
    var result           = '';
    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}

window.onload = getMusic;